# Código del curso Curso Certificación Profesional Desarrollador Java 11 y 17

# Instructor
> [Antonio Martín Sierra](https://www.udemy.com/course/curso-certificacion-profesional-desarrollador-java-se-11/) 
 

# Udemy
* Certificación Profesional Desarrollador Java 11 y 17

## Contiene

* Preparación para el examen de certificación Oracle Desarrollador Java 11 o Java 17 Profesional
* Sintaxis del lenguaje Java
* Clases de uso general Java
* Expresiones lambda 
* Manipulación de datos mediante Streams
* Programación de aplicaciones multitarea
* Preguntas tipo exámen para practica

 

 
      

---

## Notas
 
~~~
* Repositorio solo en ** Gitlab  **

 
 ~~~


---
## Código 

 
 

---
#### Version  V.0.1 
* **V.0.1**  _>>>_  Iniciando proyecto [  on 09 MARZO, 2023 ]  
 
 